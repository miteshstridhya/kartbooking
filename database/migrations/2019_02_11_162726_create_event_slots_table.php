<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_slots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->comment('Ref:events.id');
            $table->time('start_time');
            $table->time('end_time');
            $table->smallInteger('total_slots');
            $table->smallInteger('available_slots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_slots');
    }
}
