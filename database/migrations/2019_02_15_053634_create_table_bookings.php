<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('booking_mode_id')->comment('Ref:booking_mode.id');
            $table->integer('user_id')->comment('Ref:users.id');
            $table->date('event_date');
            $table->integer('event_slot_id')->comment('Ref:event_slots.id');
            $table->string('area');
            $table->string('member_name');
            $table->string('email');
            $table->smallInteger('no_of_members');
            $table->enum('has_kids', ['Yes', 'No']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
