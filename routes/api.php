<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('events', 'API\EventController@index');
Route::get('event/details/{id}', 'API\EventController@getEventDetail');
Route::get('event/slots/{id}', 'API\EventController@getEventSlots');
Route::get('verify/slot/availability/{id}', 'API\EventController@verifySlotAvailability');
Route::post('booking', 'API\BookingController@book');
Route::get('booking-modes', 'API\BookingController@bookingModes');
Route::get('booking-history/{token}', 'API\BookingController@bookingHistory');
Route::get('test-email', 'API\BookingController@sendTestEmail');
Route::group(['middleware' => 'auth:api'], function(){
  Route::get('details', 'API\UserController@details');
});
