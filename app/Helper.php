<?php
/**
* Get Time slots from given dates and duration
*
* @param $duration
* @param $start
* @param $end
*/
function getEventScheduleSlots($duration, $start, $end, $offset = 10)
{
    $array_of_time = array ();
    $start_time    = strtotime ($start); //change to strtotime
    $end_time      = strtotime ($end); //change to strtotime

    $add_mins  = $duration * 60;
    $i = 0;
    while ($start_time <= $end_time) // loop between time
    {        
        if(strtotime(date ("H:i", $start_time + ($duration *  60))) <= strtotime($end)) {
            $array_of_time[$i]['start'] = date ("H:i", $start_time);
            $array_of_time[$i]['end'] = date ("H:i", $start_time + ($duration *  60));
        }
        $start_time += ($add_mins + $offset * 60); // to check endtie=me
        $i++;
    
    }
    return $array_of_time;
}

/**
* US Date Format
*
* @param $date
*/
function formatDateUS($date) {
    $myDateTime = DateTime::createFromFormat('Y-m-d', $date);
    return $myDateTime->format('m-d-Y');  
}