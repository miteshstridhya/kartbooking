<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Actions\Actionable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{   
    use Actionable,SoftDeletes;
    
    protected $casts = [
        'event_date'  => 'date:d.m.Y'
    ];

    public function event_slot()
    {
        return $this->belongsTo('App\EventSlot', 'event_slot_id', 'id');
    }

    public function booking_mode()
    {
        return $this->belongsTo('App\BookingMode', 'booking_mode_id');
    }
}