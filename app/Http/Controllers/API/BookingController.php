<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\BookingMode;
use App\EventSlot;
use App\Booking;
use App\User;
use App\Rules\AddBookingSlot;
use Illuminate\Support\Facades\Hash;
use Mail;
use Hashids\Hashids;
class BookingController extends Controller
{
    public function sendTestEmail(Request $request) {
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);   
        $beautymail->send('emails.booking', [], function($message)
        {
            $message
                ->from('test.tridhya@gmail.com')
                ->to('pankit.s@tridhyatech.com', 'Pankit Shah')
                ->subject('Welcome!');
        });
        dd($beautymail);
        exit;
        
        $data = Booking::selectRaw('SUM(no_of_members) as total_bookings')
        ->with('event_slot')
        ->whereHas('event_slot', function ($q) {
            $q->where('event_id', 14);
        })
        ->first();
        dd($data->total_bookings);
        $slots = getEventScheduleSlots(20, '08:00', '10:00');
        dd($slots);
        // echo 'test.tridhya@gmail.com';exit;        
        $data = [
            'from' => 'test.tridhya@gmail.com',
            'to'    => 'mitesh.s@tridhyatech.com',
            'Fname'    => 'Mitesh',
            'Lname'    => 'Suthar'
        ];

        \Mail::send('emails.booking', ['data' => $data], function ($message) use ($data) {
            $message->from($data['from'])->to($data['to'])->subject('Some body wrote to you online');
        });
    }
    /**
     * Get Booking Modes
     *
     * @return \Illuminate\Http\Response
     */
    public function bookingModes(Request $request){
        $modes =  BookingMode::get();
        $data = [];
        $data['success'] = true;
        $data['data'] = $modes;
        $status_code = 200;
        return response()->json($data, $status_code);
    }

    /**
     * Get Bookings
     *
     * @return \Illuminate\Http\Response
     */
    public function book(Request $request){
        try {
            $validatedData = Validator::make($request->all(), [
                'booking_mode_id' => 'required',
                'event_slot_id' => 'required',
                'event_date' => 'required',
                'email' => 'required|email',
                'no_of_members' => ['required', 'regex:/[0-9]+/', 'gte:1', new AddBookingSlot($request->all())],
                'terms_condition'=>'required'
            ]);
            if ($validatedData->fails()) {
                $errors = $validatedData->errors();
                return response()->json(['errors' => $validatedData->errors()], 401);
            }
            // Create User
            list($name, $ext) = explode('@', $request->input('email'));
            
            $password = Hash::make(str_random(8));
            $url_token = str_random(4);

            $hashids = new Hashids();
            $random_number_array = range(0, 100);
            shuffle($random_number_array);
            $random_number_array = array_slice($random_number_array,0,5);
            
            
            // echo $request->email;exit;
            $user = User::firstOrCreate([
                'email' => $request->email
            ], [
                'name' => $name,
                'password' => $password,
                'url_token' => json_encode($random_number_array)
            ]);
            
            $url_token = $hashids->encode(json_decode($user->url_token));
            // print_r($url_token);exit;
            
            // Create Booking for a User
            $booking                  = new Booking;
            $booking->user_id         = $user->id;
            $booking->booking_mode_id = $request->booking_mode_id;
            $booking->event_slot_id   = $request->event_slot_id;
            $booking->no_of_members   = $request->no_of_members;
            $booking->description     = $request->description;
            $booking->email           = $request->email;
            $booking->event_date      = date('Y-m-d', strtotime($request->event_date));
            $booking->has_kids        = ($request->has_kids) ? 'Yes': 'No';
            $booking->save();
            
            if($booking) {
                // Send email to user
                $data = [
                    'from' => 'booking@drifterkarts.de',
                    'to'    => $request->email,
                    'name'    => $name,
                    'total_members' => $request->no_of_members,
                    'link'    => 'http://drifterkarts.de/bookings/'.$url_token
                ];

                $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);   
                $beautymail->send('emails.booking', array('data' =>$data), function($message) use($request, $name)
                {
                    $pdf_path  = public_path('doc/Nutzungsvertrag_Drifterkarts.pdf');
                    $message
                        ->from('booking@drifterkarts.de')
                        ->to($request->email, $name)
                        ->subject('Deine Reservierung bei Drifterkarts')
                        ->attach($pdf_path);
                });
            }
            $data['success'] = true;
            $data['data'] = [];
            $status_code = 200;
        }
        catch(Exception $e) {
            $data['success'] = false;
            $data['data'] = [];
            $status_code = 401;
        }
        return response()->json($data, $status_code);
    }
    
    public function bookingHistory(Request $request, $token)
    {
        if (empty($token)) {
            return response()->json(['errors' => "Invalid request!"], 401);
        }
        $hashids = new Hashids();
        $url_token = json_encode($hashids->decode($token)); // [1]
        $user = User::where('url_token', $url_token)->first();
        if(empty($user)) {
            return response()->json(['errors' => "Invalid request!"], 401);
        }
        else {
            $bookings = Booking::select('id', 'event_date', 'email', 'no_of_members', 'has_kids', 'booking_mode_id', 'event_slot_id')->where('user_id', $user->id)
            ->with(['event_slot' => function($q) {
                $q->select('id', 'start_time', 'end_time');
            }
            ])->with(['booking_mode' => function($q) {
                $q->select('id', 'name');
            }
            ])->orderBy('event_date', 'DESC')->get();
            $data = [];
            $data['success'] = true;
            $data['data'] = $bookings;
            $status_code = 200;
            return response()->json($data, $status_code);
            // echo "<pre>";print_r($bookings);exit;
        }
    }
}
