<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\EventSlot;
use PHPUnit\Framework\Constraint\Exception;
use DatePeriod, DateTime, DateInterval;
use Carbon\Carbon;

class EventController extends Controller
{    
    /**
     * Get Events
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try {
            $events = Event::get();
            $data['success'] = true;
            $data['data'] = $events;
            $status_code = 200;
        }
        catch(Exception $e) {
            $data['success'] = false;
            $data['data'] = [];
            $status_code = 401;
        }
        return response()->json($data, $status_code);
    }
    
    /**
     * Event Details
     *
     * @return \Illuminate\Http\Response
     */
    public function getEventDetail(Request $request, $id)
    {
        try {
            $per_page = 3;
            $event = Event::where('id', $id)->first();
            $dateArr = [];
            $data['data'] = $event;
            if(!empty($event)) {
                $dates = $this->getDatesFromRange($event->start_date, $event->end_date);
                $total = count($dates);
                if(!empty($dates)) {
                    $i = $j = 0;
                    foreach($dates as $k => $date) {
                        if($k > 0 && $k % 3 == 0) {
                            $i++;
                            $j = 0;
                        }
                        $dateArr[$i][$j] = formatDateUS($date);
                        $j++;
                    }
                    $data['data']['dates'] = $dateArr;
                }
            }
            $data['success'] = true;            
            $status_code = 200;
        }
        catch(Exception $e) {
            $data['success'] = false;
            $data['data'] = [];
            $status_code = 401;
        }
        return response()->json($data, $status_code);
    }

    /**
     * Fetch dates from start date and end date
     *
     * @return \Illuminate\Http\Response
     */
    public function getDatesFromRange($start, $end, $format = 'Y-m-d'){
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) {
            if($date->format($format) < date($format))
                continue; 
            $array[] = $date->format($format); 
        }

        return $array;
    }
    

    /**
     * Event Slots
     *
     * @return \Illuminate\Http\Response
     */
    public function getEventSlots(Request $request, $booking_mode_id)
    {
        try {
            $current = Carbon::now();
            $currTime = $current->format('H:i:s');
            $currDate = $current->format('Y-m-d');

            $startDt = $request->query('start_date');
            $endDt = $request->query('end_date');
            
            $time_slots = [
                'morning' => [
                    'start' => '06:00',
                    'end' => '11:59'
                ],
                'noon' => [
                    'start' => '12:00',
                    'end' => '16:59'
                ],
                'evening' => [
                    'start' => '17:00',
                    'end' => '23:59'
                ]
            ];
            
            $retArr = [];
            while (strtotime($startDt) <= strtotime($endDt)) {
                $stDt = date ("Y-m-d", strtotime($startDt));
                
                $slotQry = EventSlot::with('event')
                ->where('booking_mode_id', $booking_mode_id)
                ->where('active', 1)
                ->where('event_date',$stDt)
                ->whereHas('event', function($q) use ($stDt) {
                    $q->whereRaw('? between start_date and end_date', [$stDt]);
                });
                $slotData = $slotQry->orderBy('start_time', 'ASC')->get();
                $current_slot = 'Morning';
                                
                foreach ($slotData as $key => $slot) {
                    if(!empty($slot->event)) {
                        if($stDt == $currDate) {
                            if($currTime >= $slot->start_time)
                                continue;
                        }

                        $slot->start_time = date('H:i', strtotime($slot->start_time));
                        $slot->slot_name = 'Morning';
                        if($slot->start_time >= $time_slots['morning']['start'] && $slot->start_time <= $time_slots['morning']['end'])
                            $slot->slot_name = 'Morning';
                        else if($slot->start_time >= $time_slots['noon']['start'] && $slot->start_time <= $time_slots['noon']['end'])
                            $slot->slot_name = 'Afternoon';
                        else if($slot->start_time >= $time_slots['evening']['start'] && $slot->start_time <= $time_slots['evening']['end'])
                            $slot->slot_name = 'Evening';
                        $bookings = $slot->getBookingSlot($slot->id, $stDt);
                        if(!empty($bookings)) {
                            $slot->total_bookings = $bookings->total_bookings;
                            $slot->total_kids = $bookings->total_kids;
                        }
                        else  {

                        }
                        $retArr[$startDt][$slot->slot_name][$key] = $slot->toArray();
                    }
                    else {
                        $retArr[$startDt] = [];
                    }
                }
                if($currTime > $time_slots['morning']['start'] && $currTime < $time_slots['morning']['end']) {
                    $current_slot = isset($retArr[$startDt]['Morning']) ? 'Morning' : 'Afternoon';
                }
                else if($currTime > $time_slots['noon']['start'] && $currTime < $time_slots['noon']['end']) {
                    $current_slot = isset($retArr[$startDt]['Afternoon']) ? 'Afternoon' : 'Evening';
                }
                else if($currTime > $time_slots['evening']['start'] && $currTime < $time_slots['evening']['end'])
                    $current_slot = 'Evening';

                $retArr[$startDt]['currentSlot'] = $current_slot;
                $retArr[$startDt]['uniqueNumber'] = rand(10, 100);
                $startDt = date ("d.m.Y", strtotime("+1 day", strtotime($startDt)));
            }
            $retArr['current_slot'] = $current_slot;
            $data['success'] = true;
            $data['data'] = $retArr;
            $status_code = 200;
        }catch(Exception $e) {
            $data['success'] = false;
            $data['data'] = [];
            $status_code = 401;
        }
        return response()->json($data, $status_code);
    }
}
