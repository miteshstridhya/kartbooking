<?php

namespace App;
use App\Booking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventSlot extends Model
{   
    use SoftDeletes;
    
    protected $appends = ['total_bookings', 'total_kids', 'slot_name'];

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function bookings() {
        return $this->hasMany('App\Booking', 'event_slot_id', 'id');
    }

    public function getBookingSlot($slot_id, $date) {
        return Booking::selectRaw("SUM(no_of_members) as total_bookings, SUM(case when has_kids='Yes' then no_of_members else 0 end) as total_kids")->where('event_slot_id', $slot_id)->where('event_date', $date)->first();
    }

    public function getTotalBookedSlot($slot_id, $date) {
        return Booking::selectRaw("SUM(no_of_members) as total_bookings")->where('event_slot_id', $slot_id)->where('event_date', $date)->first();
    }

    public function getTotalBookingsAttribute() {
        return isset($this->attributes['total_bookings']) ? $this->attributes['total_bookings'] : 0;
    }

    public function getTotalKidsAttribute() {
        return isset($this->attributes['total_kids']) ? $this->attributes['total_kids'] : 0;
    }

    public function getSlotNameAttribute() {
        return isset($this->attributes['slot_name']) ? $this->attributes['slot_name'] : '';
    }
}