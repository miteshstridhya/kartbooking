<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\EventSlot;

class AddBookingSlot implements Rule
{
    protected $additional = [];
    protected $available = 0;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($additional)
    {
        $this->additional = $additional;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $event_slot_id = $this->additional['event_slot_id'];
        $event_date = date('Y-m-d', strtotime($this->additional['event_date']));
        $event_slot = EventSlot::where('id', $event_slot_id)->first();
        $booked_slots = $event_slot->getTotalBookedSlot($event_slot_id, $event_date);
        $this->available = $event_slot->total_slots - $booked_slots->total_bookings;
        if($value > $this->available)
            return false;
        else
            return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if($this->available == 0)
            return 'Sorry, No slots are available!';
        else
            return 'Only '.$this->available.' slots are available!';
    }
}
