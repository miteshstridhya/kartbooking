<?php

namespace App\Providers;

use App\EventSlot;
use App\BookingMode;
use App\Booking;
use App\Event;
use Laravel\Passport\Passport;
// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        EventSlot::class => 'App\Policies\EventSlotPolicy',
        BookingMode::class => 'App\Policies\BookingModePolicy',
        Booking::class => 'App\Policies\BookingPolicy',
        Event::class => 'App\Policies\EventPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
    }
}
