<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use Laraning\NovaTimeField\TimeField;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;

class Event extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Event';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'start_time';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'booking_mode_id', 'start_date', 'end_date', 'start_time', 'end_time', 'no_of_members', 'slot_period', 'slot_offset'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Booking Mode', 'booking_modes', 'App\Nova\BookingMode'),
            Date::make('Start Date')->rules('required'),
            Date::make('End Date')->rules('required'),
            TimeField::make('Start Time')->rules('required', 'date_format:H:i'),
            TimeField::make('End Time')->rules('required', 'date_format:H:i', 'after:start_time'),
            Select::make('Driving Period', 'slot_period')->options([
                '10' => '10',
                '20' => '20',
                '30' => '30',
                '40' => '40',
            ])->rules('required')->hideFromIndex()->hideWhenUpdating(),
            Select::make('Offset Period', 'slot_offset')->options([
                '5' => '5',
                '10' => '10',
                '15' => '15',
                '20' => '20',
            ])->rules('required')->hideFromIndex()->hideWhenUpdating(),
            Number::make('No of Members')->rules('required', 'gte:1', 'regex:/[0-9]+/'),
            Boolean::make('Active')
            ->trueValue('1')
            ->falseValue('2')->hideFromIndex(),
            HasMany::make("Event Slots",'event_slots')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Events');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Event');
    }
}
