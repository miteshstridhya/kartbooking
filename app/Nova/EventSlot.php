<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Number;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventSlot extends Resource
{
    use SoftDeletes;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\EventSlot';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'start_time';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'start_time', 'end_time', 'total_slots', 'available_slots'
    ];

    // public static $displayInNavigation = false;

    protected $showCreateForm = false;

    // public static $defaultSort = 'start_time';

    /**
     * Default ordering for index query.
     *
     * @var array
     */
    public static $orderBy = ['start_time' => 'asc'];


    protected static function applyOrderings($query, array $orderings)
    {
        if (empty($orderings) && property_exists(static::class, 'orderBy')) {
            $orderings = static::$orderBy;
        }
        
        return parent::applyOrderings($query, $orderings);
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable()->hideFromIndex(),
            Text::make(__('Event Date'), 'event_date')->sortable()->hideWhenUpdating(),
            Text::make(__('Start Time'), 'start_time')->sortable()->hideWhenUpdating(),
            Text::make(__('End Time'), 'end_time')->sortable()->hideWhenUpdating(),
            Text::make(__('Total Slots'), 'total_slots')->sortable()->hideWhenUpdating(),
            Boolean::make(__('Active'), 'active')->trueValue('1')
            ->falseValue('2'),
            // Text::make(__('Available Slots'))->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\SlotDate,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
    
    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Slots');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Slot');
    }
}
