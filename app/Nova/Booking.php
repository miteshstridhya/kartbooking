<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Rules\EditBookingSlot;
use philperusse\NovaTooltipField\Tooltip;

class Booking extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Booking';
    public static $defaultSort = 'event_slot';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'email';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'email', 'no_of_members',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    
    public static $indexDefaultOrder = [
      'es.start_time' => 'ASC'
    ];
    public static function indexQuery(NovaRequest $request, $query)
    {
        if (empty($request->get('orderBy'))) {
            $query->leftJoin('event_slots as es','bookings.event_slot_id','=','es.id');
            $query->getQuery()->orders = [];
            $query->select('bookings.*');
            return $query->orderBy(key(static::$indexDefaultOrder), reset(static::$indexDefaultOrder));
        }
        return $query;
    }

    public function fields(Request $request)
    {
        return [
            // ID::make()->sortable(),
            BelongsTo::make(__('Mode'), 'booking_mode', 'App\Nova\BookingMode'),
            Date::make(__('Event Date'), 'event_date')->sortable(),  
            BelongsTo::make(__('Event Slot'), 'event_slot', 'App\Nova\EventSlot'),
            Text::make(__('Email'), 'email')->sortable(),
            Tooltip::make(__('Description'), function(){
                return $this->description;
            }),
            Textarea::make(__('Description'), 'description')->alwaysShow()->hideFromIndex(),
            Number::make(__('No Of Members'), 'no_of_members')->sortable()->rules(['required', new EditBookingSlot($request->all())]),
            Select::make(__('Has Kids'), 'has_kids')->options([
                'Yes' => 'Yes',
                'No' => 'No'
            ])->sortable()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\BookingDateFilter,
            new Filters\BookingMode,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function relatableEventSlots(NovaRequest $request, $query)
    {
        $slot = \App\EventSlot::find($request->current);
        if(!empty($slot)) {
            return $query->where('booking_mode_id', $slot->booking_mode_id);
        }
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Bookings');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Bookings');
    }
}
