<?php

namespace App\Observers;

use App\Event;
use DateTime;
use DateInterval;
use DatePeriod;

class EventObserver
{
    /**
     * Handle the event "created" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function created(Event $event)
    {
        $eventStartDate     = new DateTime($event->start_date);
        $eventEndDate       = new DateTime($event->end_date);        
        // $event->slot_period = $event->slot_period;

        if(!empty($event->slot_period) && !empty($event->slot_period) && !empty($event->slot_period)) {
            $slots = getEventScheduleSlots($event->slot_period, $event->start_time, $event->end_time, $event->slot_offset);
            if(!empty($slots)) {
                for($i = $eventStartDate; $i <= $eventEndDate; $i->modify('+1 day')){
                    $slotInsertArray = [];
                    foreach($slots as $k => $slot) {
                        $slotInsertArray[$k]['event_id']        = $event->id;
                        $slotInsertArray[$k]['booking_mode_id'] = $event->booking_mode_id;
                        $slotInsertArray[$k]['start_time']      = $slot['start'];
                        $slotInsertArray[$k]['end_time']        = $slot['end'];
                        $slotInsertArray[$k]['total_slots']     = $event->no_of_members;
                        $slotInsertArray[$k]['available_slots'] = $event->no_of_members;
                        $slotInsertArray[$k]['event_date']      = $i->format("Y-m-d");
                    }
                    $event->event_slots()->insert($slotInsertArray);
                }
            }
        }
    }

    /**
     * Handle the event "updated" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function updated(Event $event)
    {
        $event->event_slots()->delete();
        $eventStartDate     = new DateTime($event->start_date);
        $eventEndDate       = new DateTime($event->end_date);

        if(!empty($event->slot_period) && !empty($event->slot_period) && !empty($event->slot_period)) {
            $slots = getEventScheduleSlots($event->slot_period, $event->start_time, $event->end_time, $event->slot_offset);
            if(!empty($slots)) {
                for($i = $eventStartDate; $i <= $eventEndDate; $i->modify('+1 day')){
                    $slotInsertArray = [];
                    foreach($slots as $k => $slot) {
                        $slotInsertArray[$k]['event_id'] = $event->id;
                        $slotInsertArray[$k]['booking_mode_id'] = $event->booking_mode_id;
                        $slotInsertArray[$k]['start_time'] = $slot['start'];
                        $slotInsertArray[$k]['end_time'] = $slot['end'];
                        $slotInsertArray[$k]['total_slots'] = $event->no_of_members;
                        $slotInsertArray[$k]['available_slots'] = $event->no_of_members;
                        $slotInsertArray[$k]['event_date']      = $i->format("Y-m-d");
                    }
                    $event->event_slots()->insert($slotInsertArray);
                }
            }
        }
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function deleted(Event $event)
    {
        $event->event_slots()->delete();
    }

    /**
     * Handle the event "restored" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function restored(Event $event)
    {
        //
    }

    /**
     * Handle the event "force deleted" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function forceDeleted(Event $event)
    {
        //
    }
}
