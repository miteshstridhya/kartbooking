<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    /**
	 * The primary key for the model.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_mode_id', 'start_time', 'end_time', 'slot_period', 'no_of_members', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $casts = [
        'start_date'  => 'date:Y-m-d',
        'end_date'  => 'date:Y-m-d',
        'desc' => 'array'
    ];

    public function event_slots()
    {
        return $this->hasMany('App\EventSlot', 'event_id', 'id');
    }

    public function booking_modes()
    {
        return $this->belongsTo('App\BookingMode', 'booking_mode_id');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }
}
