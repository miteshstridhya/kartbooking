<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingMode extends Model
{
    public function event()
    {
        return $this->hasOne('App\Event', 'booking_mode_id');
    }

    public function event_slots()
    {
        return $this->belongsTo('App\EventSlot', 'booking_mode_id');
    }
}
