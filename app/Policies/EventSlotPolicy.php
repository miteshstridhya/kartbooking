<?php

namespace App\Policies;

use App\Admin;
use App\EventSlot;
use App\Booking;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventSlotPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the Admin can view the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\EventSlot  $eventSlot
     * @return mixed
     */
    public function view(Admin $admin, EventSlot $eventSlot)
    {
        return true;
    }

    /**
     * Determine whether the Admin can create event slots.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        // return false;
    }

    /**
     * Determine whether the Admin can update the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\EventSlot  $eventSlot
     * @return mixed
     */
    public function update(Admin $admin, EventSlot $eventSlot)
    {
        $id = $eventSlot->id;
        $data = Booking::selectRaw('SUM(no_of_members) as total_bookings')->where('event_slot_id', $id)->first();

        if($data->total_bookings > 0)
            return false;
        else
            return true;
        // return true;
    }

    /**
     * Determine whether the Admin can delete the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\EventSlot  $eventSlot
     * @return mixed
     */
    public function delete(Admin $admin, EventSlot $eventSlot)
    {
        $id = $eventSlot->id;
        $data = Booking::selectRaw('SUM(no_of_members) as total_bookings')->where('event_slot_id', $id)->first();

        if($data->total_bookings > 0)
            return false;
        else
            return true;
    }

    /**
     * Determine whether the Admin can restore the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\EventSlot  $eventSlot
     * @return mixed
     */
    public function restore(Admin $admin, EventSlot $eventSlot)
    {
        return false;
    }

    /**
     * Determine whether the Admin can permanently delete the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\EventSlot  $eventSlot
     * @return mixed
     */
    public function forceDelete(Admin $admin, EventSlot $eventSlot)
    {
        return false;
    }
}
