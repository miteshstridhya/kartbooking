<?php

namespace App\Policies;

use App\Admin;
use App\Event;
use App\Booking;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the admin can view the event.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Event  $event
     * @return mixed
     */
    public function view(Admin $admin, Event $event)
    {
        return true;
    }

    /**
     * Determine whether the admin can create events.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return true;
    }

    /**
     * Determine whether the admin can update the event.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Event  $event
     * @return mixed
     */
    public function update(Admin $admin, Event $event)
    {
        $id = $event->id;
        $data = Booking::selectRaw('SUM(no_of_members) as total_bookings')
        ->with('event_slot')
        ->whereHas('event_slot', function ($q) use($id) {
            $q->where('event_id', $id);
        })
        ->first();

        if($data->total_bookings > 0)
            return false;
        else
            return true;
    }

    /**
     * Determine whether the admin can delete the event.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Event  $event
     * @return mixed
     */
    public function delete(Admin $admin, Event $event)
    {
        $id = $event->id;
        $data = Booking::selectRaw('SUM(no_of_members) as total_bookings')
        ->with('event_slot')
        ->whereHas('event_slot', function ($q) use($id) {
            $q->where('event_id', $id);
        })
        ->first();

        if($data->total_bookings > 0)
            return false;
        else
            return true;
    }

    /**
     * Determine whether the admin can restore the event.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Event  $event
     * @return mixed
     */
    public function restore(Admin $admin, Event $event)
    {
        return true;
    }

    /**
     * Determine whether the admin can permanently delete the event.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Event  $event
     * @return mixed
     */
    public function forceDelete(Admin $admin, Event $event)
    {
        $id = $event->id;
        $data = Booking::selectRaw('SUM(no_of_members) as total_bookings')
        ->with('event_slot')
        ->whereHas('event_slot', function ($q) use($id) {
            $q->where('event_id', $id);
        })
        ->first();

        if($data->total_bookings > 0)
            return false;
        else
            return true;
    }
}
