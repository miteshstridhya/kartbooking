<?php

namespace App\Policies;

use App\Admin;
use App\Booking;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the Admin can view the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Booking  $booking
     * @return mixed
     */
    public function view(Admin $admin, Booking $booking)
    {
        return true;
    }

    /**
     * Determine whether the Admin can create event slots.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return false;
    }

    /**
     * Determine whether the Admin can update the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Booking  $booking
     * @return mixed
     */
    public function update(Admin $admin, Booking $booking)
    {
        return true;
    }

    /**
     * Determine whether the Admin can delete the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Booking  $booking
     * @return mixed
     */
    public function delete(Admin $admin, Booking $booking)
    {
        return true;
    }

    /**
     * Determine whether the Admin can restore the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Booking  $booking
     * @return mixed
     */
    public function restore(Admin $admin, Booking $booking)
    {
        return true;
    }

    /**
     * Determine whether the Admin can permanently delete the event slot.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Booking  $booking
     * @return mixed
     */
    public function forceDelete(Admin $admin, Booking $booking)
    {
        return true;
    }
}
