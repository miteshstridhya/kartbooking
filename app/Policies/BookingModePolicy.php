<?php

namespace App\Policies;

use App\Admin;
use App\BookingMode;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingModePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the admin can view the booking mode.
     *
     * @param  \App\Admin  $admin
     * @param  \App\BookingMode  $bookingMode
     * @return mixed
     */
    public function view(Admin $admin, BookingMode $bookingMode)
    {
        return true;
    }

    /**
     * Determine whether the admin can create booking modes.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return false;
    }

    /**
     * Determine whether the admin can update the booking mode.
     *
     * @param  \App\Admin  $admin
     * @param  \App\BookingMode  $bookingMode
     * @return mixed
     */
    public function update(Admin $admin, BookingMode $bookingMode)
    {
        return false;
    }

    /**
     * Determine whether the admin can delete the booking mode.
     *
     * @param  \App\Admin  $admin
     * @param  \App\BookingMode  $bookingMode
     * @return mixed
     */
    public function delete(Admin $admin, BookingMode $bookingMode)
    {
        return false;
    }

    /**
     * Determine whether the admin can restore the booking mode.
     *
     * @param  \App\Admin  $admin
     * @param  \App\BookingMode  $bookingMode
     * @return mixed
     */
    public function restore(Admin $admin, BookingMode $bookingMode)
    {
        return false;
    }

    /**
     * Determine whether the admin can permanently delete the booking mode.
     *
     * @param  \App\Admin  $admin
     * @param  \App\BookingMode  $bookingMode
     * @return mixed
     */
    public function forceDelete(Admin $admin, BookingMode $bookingMode)
    {
        return false;
    }
}
