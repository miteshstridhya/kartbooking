@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title">
				Hey Drifter,
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Vielen Dank für Deine Reservierung! Deine {{$data['total_members']}} Karts fahren gerade in die Boxengasse und stehen dann für dich bereit.
				<br /><br />
				Wir freuen uns auf Deinen Besuch.
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		<tr>
			<td>
				@include('beautymail::templates.minty.button', ['text' => 'Meine Buchungen', 'link' => $data['link']])
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		<table class="table table-bordered table-dark border-2">
			<thead>
			 <tr>
				 <th class="border-2">Altersgruppe</th>
				 <th class="border-2">Mitzubringende/ Unterschriebene Dokumente</th>
				 <th class="border-2">Geschwindigkeit</th>
			 </tr>
			</thead> 
			<tbody>
			  <tr>
				 <td>0-9 Jahre</td>
				 <td colspan="2">keine Teilnahme möglich</td>
			 </tr>
			  <tr>
				 <td>10 - 13 Jahre</td>
				 <td><a href="/../doc/Nutzungsvertrag_Haftungsausschluss.pdf" class="foot-link footer_link" target="__blank">Nutzungsvereinbarung</a> + <a href="/../doc/Begleitperson.pdf" class="foot-link footer_link" target="__blank">Begleitperson</a></td>
				 <td>eingeschränkt</td>
			 </tr>
			 <tr>
				 <td>14 - 15 Jahre</td>
				 <td><a href="/../doc/Nutzungsvertrag_Haftungsausschluss.pdf" class="foot-link footer_link" target="__blank">Nutzungsvereinbarung</a></td>
				 <td>eingeschränkt</td>
			 </tr>
			  <tr>
				 <td>16 - 17 Jahre</td>
				 <td><a href="/../doc/Nutzungsvertrag_Haftungsausschluss.pdf" class="foot-link footer_link" target="__blank">Nutzungsvereinbarung</a></td>
				 <td>voll</td>
			 </tr>
			 <tr>
				 <td>ab 18 Jahre</td>
				 <td>keine</td>
				 <td>voll</td>
				 
			 </tr>
			</tbody>
			</table>
	@include('beautymail::templates.minty.contentEnd')

@stop
