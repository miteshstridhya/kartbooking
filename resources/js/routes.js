import Home from './components/Home.vue';
import Bookings from './components/Bookings.vue';
import BookingHistory from './components/BookingHistory.vue';
import Impressum from './components/Impressum.vue';
import Datenschutz from './components/Datenschutz.vue';
import Faq from './components/Faq.vue';
import Altersbeschränkung from './components/Altersbeschränkung.vue';

export const routes = [
    { 
        path: '/', 
        component: Home, 
        name: 'Home' 
    },
    { 
        path: '/startzeit-buchen', 
        component: Bookings, 
        name: 'Bookings',
        redirect: '/startzeit-buchen/racing_mode'
    },
    { 
        path: '/startzeit-buchen/:mode', 
        component: Bookings, 
        name: 'Bookings' 
    },
    {
        path: '/impressum',
        component: Impressum,
        name: 'Impressum'
    },
    {
        path: '/datenschutz',
        component: Datenschutz,
        name: 'Datenschutz'
    },
    {
        path: '/bookings/:token',
        component: BookingHistory,
        name: 'BookingHistory'
    },
    {
        path: '/faq',
        component: Faq,
        name: 'Faq'
    },
    {
        path: '/altersbeschränkung',
        component: Altersbeschränkung,
        name: 'Altersbeschränkung'
    }
];