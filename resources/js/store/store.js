import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
        
    state: {
        currentSlot: [],
        bookingMode: [],
        email:''
    },
    getters : {
        CURRENT_SLOT : state => {
            return state.currentSlot;
        },
        BOOKING_MODE : state => {
            return state.bookingMode;
        },
        EMAIL : state => {
            
            return state.email;
        }
    },
    mutations: {
        SET_CURRENT_SLOT : (state,payload) => {
            state.currentSlot = payload
        },
        SET_BOOKING_MODE : (state,payload) => {
            state.bookingMode = payload
        },
        SET_EMAIL : (state,payload) => {
            state.email = payload
        }
    },
    actions:{
        SAVE_CURRENT_SLOT : async (context,payload) => {
            context.commit('SET_CURRENT_SLOT',payload)
        },
        SAVE_BOOKING_MODE : async (context,payload) => {
            context.commit('SET_BOOKING_MODE',payload)
        },
        SAVE_EMAIL : async (context,payload) => {
            context.commit('SET_EMAIL',payload)
        }
    },
});

// export default store;