import '@babel/polyfill'  
import Vue from 'vue'
import VueRouter from 'vue-router';
import store from './store/store';
Vue.use(VueRouter);

import Vuex from 'vuex'
Vue.use(Vuex)

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

import VueCookies from 'vue-cookies';
Vue.use(VueCookies);

Vue.use(VueInternationalization);

import { AccordionPlugin } from '@syncfusion/ej2-vue-navigations';
Vue.use(AccordionPlugin);


// const lang = document.documentElement.lang.substr(0, 2); 
const lang = 'de';
// or however you determine your current app locale

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

import { routes } from './routes';

import Axios from 'axios'
Vue.prototype.$http = Axios;
import validationMessagesEN from 'vee-validate/dist/locale/en';
import validationMessagesDE from 'vee-validate/dist/locale/de'; 

import VeeValidate from 'vee-validate'
Vue.use(VeeValidate, {
    i18nRootKey: 'validations', // customize the root path for validation messages.
    i18n,
    dictionary: {
     en: validationMessagesEN,
     de: validationMessagesDE
    }
})

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);
window.Swal = require('sweetalert2') // added here
 
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
import validationMessages from 'vee-validate/dist/locale/en';

//Vue.prototype.$apiURL = 'http://kartbooking.localhost/api/' 
//Vue.prototype.$apiURL = 'http://127.0.0.1:8000/api/'
Vue.prototype.$apiURL = 'https://drifterkarts.de/api/'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)


import HeaderComponent from "./components/HeaderComponent.vue"
import FooterComponent from "./components/FooterComponent.vue"
import Navbar from "./components/Navbar.vue"



const router = new VueRouter({
    mode: 'history',
    routes
});

Vue.component('route-header', HeaderComponent);
Vue.component('route-footer', FooterComponent);

Vue.component('Navbar', Navbar);
new Vue({
    el: '#app',
    store: store,
    i18n,
    router
});