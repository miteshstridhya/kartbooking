setTimeout(function(){ $('.morph').removeClass('d-none'); }, 1500);
jQuery(document).ready(function($) {
	$('.testimonials-silde').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    items:1,
	    dots:true
	});
	$('.video_slider').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    items:1,
	    dots:false,
	    autoplay:false,
	});
	$("#js-rotating").Morphext({
	    animation: "fadeIn",
	    separator: ",",
	    speed: 2600
	});
});



$(window).scroll(function(){
    if ($(window).scrollTop() >=50) {
        $('.main-header').addClass('sticky');
    }
    else {
        $('.main-header').removeClass('sticky');
    }
});

