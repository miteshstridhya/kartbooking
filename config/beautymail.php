<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#004ca3',
        'button'    => '#004cad',

    ],

    'view' => [
        'senderName'  => 'Test Tridhya',
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => 'test.tridhya@gmail.com',

        'logo'        => [
            'path'   => '%PUBLIC%/vendor/beautymail/assets/images/sunny/logo.png',
            'width'  => '291',
            'height' => '76',
        ],

        'twitter'  => null,
        'facebook' => null,
        'flickr'   => null,
    ],

];
